const hi = 0;
const hhh = hi;
const hhhh = hi;  
const hii = {
    "title": "The Favourite",
    "genres": [
      "Drama",
      "History"
    ],
    "runtime": 121,
    "rated": "R",
    "year": 2018,
    "directors": [
      "Yorgos Lanthimos"
    ],
    "cast": [
      "Olivia Colman",
      "Emma Stone",
      "Rachel Weisz"
    ],
    "type": "movie"
  }